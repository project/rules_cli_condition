<?php

namespace Drupal\rules_cli_condition\Plugin\Condition;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rules\Core\Attribute\Condition;
use Drupal\rules\Core\RulesConditionBase;

/**
 * Provides an 'Command-line environment' condition.
 *
 * @Condition(
 *   id = "rules_cli_condition",
 *   label = @Translation("Command-line environment"),
 *   category = @Translation("System"),
 * )
 */
#[Condition(
  id: "rules_cli_condition",
  label: new TranslatableMarkup("Command-line environment"),
  category: new TranslatableMarkup("System"),
)]
class RulesCliCondition extends RulesConditionBase {

  /**
   * Check if system environment is CLI.
   *
   * @return bool
   *   TRUE if system environment is CLI.
   */
  protected function doEvaluate() {
    return PHP_SAPI === 'cli';
  }
}
